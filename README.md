# UDP stack

Řešení jednoduchého hardwarového UDP stacku. 
Součástí je také ukázka použití s jednoduchým jádrem, které převádí malá písmena na velká. 
Předpokladem úspěšné implementace je potřeba mít nainstalované vivado 2017.3 a vyšší.

Součástí ukázkového designu je také jádro JTAG to AXI lite, kterým se nastavuje UDP vrstva. Ukazka tcl prikazu pro komunikaci s timto jadrem je v souboru commands.tcl

