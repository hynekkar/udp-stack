
#setting clock domain on AXIs

set_property CONFIG.FREQ_HZ 125000000 [get_bd_intf_pins /upd_stack_0/mac_rx]
set_property CONFIG.CLK_DOMAIN {CONFIG.CLK_DOMAIN	/tri_mode_ethernet_mac_0/rx_mac_aclk} [get_bd_intf_pins /upd_stack_0/mac_rx]
set_property CONFIG.CLK_DOMAIN /tri_mode_ethernet_mac_0/rx_mac_aclk [get_bd_intf_pins /upd_stack_0/mac_rx]
set_property CONFIG.CLK_DOMAIN TopBlock_clk_wiz_0_0_clk_out1 [get_bd_intf_pins /upd_stack_0/mac_tx]
set_property CONFIG.FREQ_HZ 125000000 [get_bd_intf_pins /upd_stack_0/mac_tx]



#creating transactions
create_hw_axi_txn app_our_ip [get_hw_axis hw_axi_1] -address 0 -type read -force
create_hw_axi_txn app_mac_1 [get_hw_axis hw_axi_1] -address 4 -type read -force
create_hw_axi_txn app_mac_2 [get_hw_axis hw_axi_1] -address 8 -type read -force
create_hw_axi_txn app_dst_ip [get_hw_axis hw_axi_1] -address C -type read -force
create_hw_axi_txn app_dst_port [get_hw_axis hw_axi_1] -address 10 -type read -force
create_hw_axi_txn app_src_port [get_hw_axis hw_axi_1] -address 14 -type read -force
create_hw_axi_txn app_filter_port [get_hw_axis hw_axi_1] -address 18 -type read -force


#command for writing data to register
create_hw_axi_txn app_filter_port_set [get_hw_axis hw_axi_1] -address 24 -type write -data 9200


#reading commands
run_hw_axi app_our_ip
run_hw_axi app_mac_1
run_hw_axi app_mac_2
run_hw_axi app_dst_ip
run_hw_axi app_dst_port
run_hw_axi app_src_port
run_hw_axi app_filter_port

