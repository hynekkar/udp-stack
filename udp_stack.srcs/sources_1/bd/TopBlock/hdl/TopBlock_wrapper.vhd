--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.3 (lin64) Build 2018833 Wed Oct  4 19:58:07 MDT 2017
--Date        : Thu Apr 18 09:52:13 2019
--Host        : karel-ubuntu running 64-bit Ubuntu 16.04.5 LTS
--Command     : generate_target TopBlock_wrapper.bd
--Design      : TopBlock_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity TopBlock_wrapper is
  port (
    default_sysclk_250_clk_n : in STD_LOGIC;
    default_sysclk_250_clk_p : in STD_LOGIC;
    phy_rst_n : out STD_LOGIC_VECTOR ( 0 to 0 );
    reset : in STD_LOGIC;
    rgmii_rd : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rgmii_rx_ctl : in STD_LOGIC;
    rgmii_rxc : in STD_LOGIC;
    rgmii_td : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rgmii_tx_ctl : out STD_LOGIC;
    rgmii_txc : out STD_LOGIC
  );
end TopBlock_wrapper;

architecture STRUCTURE of TopBlock_wrapper is
  component TopBlock is
  port (
    default_sysclk_250_clk_n : in STD_LOGIC;
    default_sysclk_250_clk_p : in STD_LOGIC;
    rgmii_rd : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rgmii_rx_ctl : in STD_LOGIC;
    rgmii_rxc : in STD_LOGIC;
    rgmii_td : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rgmii_tx_ctl : out STD_LOGIC;
    rgmii_txc : out STD_LOGIC;
    reset : in STD_LOGIC;
    phy_rst_n : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component TopBlock;
begin
TopBlock_i: component TopBlock
     port map (
      default_sysclk_250_clk_n => default_sysclk_250_clk_n,
      default_sysclk_250_clk_p => default_sysclk_250_clk_p,
      phy_rst_n(0) => phy_rst_n(0),
      reset => reset,
      rgmii_rd(3 downto 0) => rgmii_rd(3 downto 0),
      rgmii_rx_ctl => rgmii_rx_ctl,
      rgmii_rxc => rgmii_rxc,
      rgmii_td(3 downto 0) => rgmii_td(3 downto 0),
      rgmii_tx_ctl => rgmii_tx_ctl,
      rgmii_txc => rgmii_txc
    );
end STRUCTURE;
