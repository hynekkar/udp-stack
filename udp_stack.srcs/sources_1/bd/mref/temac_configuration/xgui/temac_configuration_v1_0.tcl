# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "MAC_SPEED" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RUN_HALF_DUPLEX" -parent ${Page_0}


}

proc update_PARAM_VALUE.MAC_SPEED { PARAM_VALUE.MAC_SPEED } {
	# Procedure called to update MAC_SPEED when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAC_SPEED { PARAM_VALUE.MAC_SPEED } {
	# Procedure called to validate MAC_SPEED
	return true
}

proc update_PARAM_VALUE.RUN_HALF_DUPLEX { PARAM_VALUE.RUN_HALF_DUPLEX } {
	# Procedure called to update RUN_HALF_DUPLEX when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RUN_HALF_DUPLEX { PARAM_VALUE.RUN_HALF_DUPLEX } {
	# Procedure called to validate RUN_HALF_DUPLEX
	return true
}


proc update_MODELPARAM_VALUE.MAC_SPEED { MODELPARAM_VALUE.MAC_SPEED PARAM_VALUE.MAC_SPEED } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAC_SPEED}] ${MODELPARAM_VALUE.MAC_SPEED}
}

proc update_MODELPARAM_VALUE.RUN_HALF_DUPLEX { MODELPARAM_VALUE.RUN_HALF_DUPLEX PARAM_VALUE.RUN_HALF_DUPLEX } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RUN_HALF_DUPLEX}] ${MODELPARAM_VALUE.RUN_HALF_DUPLEX}
}

