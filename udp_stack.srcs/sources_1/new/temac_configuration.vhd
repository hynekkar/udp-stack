----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 02/25/2019 02:38:04 PM
-- Design Name:
-- Module Name: temac_configuration - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity temac_configuration is
  generic(
    MAC_SPEED : std_logic_vector(1 downto 0)  := "10";
    RUN_HALF_DUPLEX      : std_logic := '0'
  );
  port(
    gtx_clk                 : in  std_logic;
    gtx_resetn              : in  std_logic;
    rx_configuration_vector : out std_logic_vector(79 downto 0);
    tx_configuration_vector : out std_logic_vector(79 downto 0)
  );
end temac_configuration;

architecture Behavioral of temac_configuration is

     type state_typ is     (STARTUP,
                            RESET_MAC,
                            DONE);
     ---------------------------------------------------
     -- Signal declarations
     signal control_status             : state_typ;
     signal count_shift                : std_logic_vector(20 downto 0) := (others => '0');

     signal tx_reset                   : std_logic;
     signal tx_enable                  : std_logic;
     signal tx_vlan_enable             : std_logic;
     signal tx_fcs_enable              : std_logic;
     signal tx_jumbo_enable            : std_logic;
     signal tx_fc_enable               : std_logic;
     signal tx_hd_enable               : std_logic;
     signal tx_ifg_adjust              : std_logic;
     signal tx_speed                   : std_logic_vector(1 downto 0) := (others => '0');
     signal tx_max_frame_enable        : std_logic;
     signal tx_max_frame_length        : std_logic_vector(14 downto 0);
     signal tx_pause_addr              : std_logic_vector(47 downto 0);

     signal rx_reset                   : std_logic;
     signal rx_enable                  : std_logic;
     signal rx_vlan_enable             : std_logic;
     signal rx_fcs_enable              : std_logic;
     signal rx_jumbo_enable            : std_logic;
     signal rx_fc_enable               : std_logic;
     signal rx_hd_enable               : std_logic;
     signal rx_len_type_chk_disable    : std_logic;
     signal rx_control_len_chk_dis     : std_logic;
     signal rx_promiscuous             : std_logic;
     signal rx_speed                   : std_logic_vector(1 downto 0);
     signal rx_max_frame_enable        : std_logic;
     signal rx_max_frame_length        : std_logic_vector(14 downto 0);
     signal rx_pause_addr              : std_logic_vector(47 downto 0);

     signal gtx_reset                  : std_logic;
begin


     gtx_reset <= not gtx_resetn;

     rx_configuration_vector <= rx_pause_addr &
                                 '0' & rx_max_frame_length &
                                 '0' & rx_max_frame_enable &
                                 rx_speed &
                                 rx_promiscuous &
                                 '0' & rx_control_len_chk_dis &
                                 rx_len_type_chk_disable &
                                 '0' & rx_hd_enable &
                                 rx_fc_enable &
                                 rx_jumbo_enable &
                                 rx_fcs_enable &
                                 rx_vlan_enable &
                                 rx_enable &
                                 rx_reset;

     tx_configuration_vector <= tx_pause_addr &
                                 '0' & tx_max_frame_length &
                                 '0' & tx_max_frame_enable &
                                 tx_speed &
                                 "000" & tx_ifg_adjust &
                                 '0' & tx_hd_enable &
                                 tx_fc_enable &
                                 tx_jumbo_enable &
                                 tx_fcs_enable &
                                 tx_vlan_enable &
                                 tx_enable &
                                 tx_reset;

     -- don't reset this  - it will always be updated before it is used..
     -- it does need an init value (zero)
     gen_count : process (gtx_clk)
     begin
        if gtx_clk'event and gtx_clk = '1' then
          count_shift <= count_shift(19 downto 0) & (gtx_reset or tx_reset);
        end if;
     end process gen_count;

     ------------------------------------------------------------------------------
     -- Management process. This process sets up the configuration by
     -- turning off flow control
     ------------------------------------------------------------------------------
     gen_state : process (gtx_clk)
     begin
        if gtx_clk'event and gtx_clk = '1' then
           if gtx_reset = '1' then
              tx_reset                <= '0';
              tx_enable               <= '1';
              tx_vlan_enable          <= '0';
              tx_fcs_enable           <= '0';
              tx_jumbo_enable         <= '0';
              tx_fc_enable            <= '1';
              tx_hd_enable            <= RUN_HALF_DUPLEX;
              tx_ifg_adjust           <= '0';
              tx_speed                <= MAC_SPEED;
              tx_max_frame_enable     <= '0';
              tx_max_frame_length     <= (others => '0');
              tx_pause_addr           <= X"0605040302DA";

              rx_reset                <= '0';
              rx_enable               <= '1';
              rx_vlan_enable          <= '0';
              rx_fcs_enable           <= '0';
              rx_jumbo_enable         <= '0';
              rx_fc_enable            <= '1';
              rx_hd_enable            <= RUN_HALF_DUPLEX;
              rx_len_type_chk_disable <= '0';
              rx_control_len_chk_dis  <= '0';

              rx_promiscuous          <= '0';

              rx_speed                <= MAC_SPEED;
              rx_max_frame_enable     <= '0';
              rx_max_frame_length     <= (others => '0');
              rx_pause_addr           <= X"0605040302DA";
              control_status          <= STARTUP;

           -- main state machine is kicking off multi cycle accesses in each state so has to
           -- stall while they take place
           else
              case control_status is
                 when STARTUP =>
                    -- this state will be ran after reset to wait for count_shift
                    if count_shift(20) = '0' then
                       control_status <= RESET_MAC;
                    end if;
                 when RESET_MAC =>
                    tx_reset       <= '1';
                    rx_reset       <= '1';
                    rx_speed       <= MAC_SPEED;
                    tx_speed       <= MAC_SPEED;
                    control_status <= DONE;
                 when DONE =>
                    tx_reset       <= '0';
                    rx_reset       <= '0';
                 when others =>
                    control_status  <= STARTUP;
              end case;
           end if;
        end if;
      end process gen_state;
end Behavioral;
