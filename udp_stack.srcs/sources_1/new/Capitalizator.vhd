----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 02/28/2019 10:19:27 AM
-- Design Name:
-- Module Name: Capitalizator - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Capitalizator is
  port(
    s_axis_tvalid : IN STD_LOGIC;
    s_axis_tready : OUT STD_LOGIC;
    s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_tlast : IN STD_LOGIC;
    m_axis_tvalid : OUT STD_LOGIC;
    m_axis_tready : IN STD_LOGIC;
    m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_tlast : OUT STD_LOGIC
  );
end Capitalizator;

architecture Behavioral of Capitalizator is

begin
  s_axis_tready <= m_axis_tready;
  m_axis_tvalid <= s_axis_tvalid;
  m_axis_tlast <= s_axis_tlast;

  CAPITALIZATION : process(s_axis_tdata)
  begin
    if(unsigned(s_axis_tdata) >= 97 and unsigned(s_axis_tdata) <= 122) then
      m_axis_tdata <= std_logic_vector(unsigned(s_axis_tdata) - 32);
    else
      m_axis_tdata <= s_axis_tdata;
    end if;
  end process; -- end CAPITALIZATION
end Behavioral;
