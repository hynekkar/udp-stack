----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 02/25/2019 11:17:09 AM
-- Design Name:
-- Module Name: upd_stack - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- set_property CONFIG.FREQ_HZ 125000000 [get_bd_intf_pins /upd_stack_0/mac_rx]
-- set_property CONFIG.CLK_DOMAIN {CONFIG.CLK_DOMAIN	/tri_mode_ethernet_mac_0/rx_mac_aclk} [get_bd_intf_pins /upd_stack_0/mac_rx]
-- set_property CONFIG.CLK_DOMAIN /tri_mode_ethernet_mac_0/rx_mac_aclk [get_bd_intf_pins /upd_stack_0/mac_rx]
-- set_property CONFIG.CLK_DOMAIN TopBlock_clk_wiz_0_0_clk_out1 [get_bd_intf_pins /upd_stack_0/mac_tx]
-- set_property CONFIG.FREQ_HZ 125000000 [get_bd_intf_pins /upd_stack_0/mac_tx]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;
use ieee.std_logic_misc.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;
use work.arp;
use work.arpv2;

entity upd_stack is
  generic (
    C_S_AXI_DATA_WIDTH    : integer := 32;
    C_S_AXI_ADDR_WIDTH    : integer := 32
  );
  port(
    rx_clk               : in  std_logic;
    tx_clk               : in  std_logic;
    reset                : in  std_logic;
    mac_tx_rstn          : in  std_logic;
    mac_tx_tdata         : out std_logic_vector(7 downto 0);
    mac_tx_tvalid        : out std_logic;
    mac_tx_tready        : in  std_logic;
    mac_tx_tlast         : out std_logic;
    mac_rx_rstn          : in  std_logic;
    mac_rx_tdata         : in  std_logic_vector(7 downto 0);
    mac_rx_tvalid        : in  std_logic;
    mac_rx_tready        : out std_logic;
    mac_rx_tlast         : in  std_logic;   -- indicates last byte of the trame

    app_config_ACLK      : in  std_logic;
    app_config_ARESETN   : in  std_logic;
    app_config_AWADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    app_config_AWPROT    : in  std_logic_vector(2 downto 0);
    app_config_AWVALID   : in  std_logic;
    app_config_AWREADY   : out std_logic;
    app_config_WDATA     : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    app_config_WSTRB     : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
    app_config_WVALID    : in  std_logic;
    app_config_WREADY    : out std_logic;
    app_config_BRESP     : out std_logic_vector(1 downto 0);
    app_config_BVALID    : out std_logic;
    app_config_BREADY    : in  std_logic;
    app_config_ARADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    app_config_ARPROT    : in  std_logic_vector(2 downto 0);
    app_config_ARVALID   : in  std_logic;
    app_config_ARREADY   : out std_logic;
    app_config_RDATA     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    app_config_RRESP     : out std_logic_vector(1 downto 0);
    app_config_RVALID    : out std_logic;
    app_config_RREADY    : in  std_logic
  );
end upd_stack;

architecture Behavioral of upd_stack is
  component app_axi_configuration
  generic (
    C_S_AXI_DATA_WIDTH    : integer := 32;
    C_S_AXI_ADDR_WIDTH    : integer := 32;
    C_DEF_OUR_IP_ADDRESS  : std_logic_vector(31 downto 0);
    C_DEF_OUR_MAC_ADDRESS : std_logic_vector(47 downto 0);
    C_DEF_DST_IP_ADDRESS  : std_logic_vector(31 downto 0);
    C_DEF_DST_PORT        : std_logic_vector(15 downto 0);
    C_DEF_SRC_PORT        : std_logic_vector(15 downto 0);
    C_DEF_FILTER_PORT        : std_logic_vector(15 downto 0)
  );
  port (
    our_ip_address  : out std_logic_vector (31 downto 0);
    our_mac_address : out std_logic_vector (47 downto 0);
    dst_ip_addr     : out std_logic_vector (31 downto 0);
    dst_port        : out std_logic_vector (15 downto 0);
    src_port        : out std_logic_vector (15 downto 0);
    filter_port        : out std_logic_vector (15 downto 0);
    S_AXI_ACLK      : in  std_logic;
    S_AXI_ARESETN   : in  std_logic;
    S_AXI_AWADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    S_AXI_AWPROT    : in  std_logic_vector(2 downto 0);
    S_AXI_AWVALID   : in  std_logic;
    S_AXI_AWREADY   : out std_logic;
    S_AXI_WDATA     : in  std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    S_AXI_WSTRB     : in  std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
    S_AXI_WVALID    : in  std_logic;
    S_AXI_WREADY    : out std_logic;
    S_AXI_BRESP     : out std_logic_vector(1 downto 0);
    S_AXI_BVALID    : out std_logic;
    S_AXI_BREADY    : in  std_logic;
    S_AXI_ARADDR    : in  std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
    S_AXI_ARPROT    : in  std_logic_vector(2 downto 0);
    S_AXI_ARVALID   : in  std_logic;
    S_AXI_ARREADY   : out std_logic;
    S_AXI_RDATA     : out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
    S_AXI_RRESP     : out std_logic_vector(1 downto 0);
    S_AXI_RVALID    : out std_logic;
    S_AXI_RREADY    : in  std_logic
  );
  end component app_axi_configuration;


  component UDP_Complete_nomac
  generic (
    CLOCK_FREQ      : integer := 125000000;
    ARP_TIMEOUT     : integer := 60;
    ARP_MAX_PKT_TMO : integer := 5;
    MAX_ARP_ENTRIES : integer := 255
  );
  port (
    udp_tx_start          : in  std_logic;
    udp_txi               : in  udp_tx_type;
    udp_tx_result         : out std_logic_vector (1 downto 0);
    udp_tx_data_out_ready : out std_logic;
    udp_rx_start          : out std_logic;
    udp_rxo               : out udp_rx_type;
    ip_rx_hdr             : out ipv4_rx_header_type;
    rx_clk                : in  STD_LOGIC;
    tx_clk                : in  STD_LOGIC;
    reset                 : in  STD_LOGIC;
    our_ip_address        : in  STD_LOGIC_VECTOR (31 downto 0);
    our_mac_address       : in  std_logic_vector (47 downto 0);
    control               : in  udp_control_type;
    arp_pkt_count         : out STD_LOGIC_VECTOR(7 downto 0);
    ip_pkt_count          : out STD_LOGIC_VECTOR(7 downto 0);
    mac_tx_tdata          : out std_logic_vector(7 downto 0);
    mac_tx_tvalid         : out std_logic;
    mac_tx_tready         : in  std_logic;
    mac_tx_tfirst         : out std_logic;
    mac_tx_tlast          : out std_logic;
    mac_rx_tdata          : in  std_logic_vector(7 downto 0);
    mac_rx_tvalid         : in  std_logic;
    mac_rx_tready         : out std_logic;
    mac_rx_tlast          : in  std_logic								-- indicates last byte of the trame
  );
  end component UDP_Complete_nomac;

  COMPONENT packet_fifo
    PORT (
      s_aclk : IN STD_LOGIC;
      s_aresetn : IN STD_LOGIC;
      s_axis_tvalid : IN STD_LOGIC;
      s_axis_tready : OUT STD_LOGIC;
      s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_tlast : IN STD_LOGIC;
      m_axis_tvalid : OUT STD_LOGIC;
      m_axis_tready : IN STD_LOGIC;
      m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_tlast : OUT STD_LOGIC;
      axis_data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
    );
  END COMPONENT;

  COMPONENT cross_clock_packet_fifo
    PORT (
      m_aclk : IN STD_LOGIC;
      s_aclk : IN STD_LOGIC;
      s_aresetn : IN STD_LOGIC;
      s_axis_tvalid : IN STD_LOGIC;
      s_axis_tready : OUT STD_LOGIC;
      s_axis_tdata : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      s_axis_tlast : IN STD_LOGIC;
      m_axis_tvalid : OUT STD_LOGIC;
      m_axis_tready : IN STD_LOGIC;
      m_axis_tdata : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      m_axis_tlast : OUT STD_LOGIC
    );
  END COMPONENT;

  component Capitalizator
  port (
    s_axis_tvalid : IN  STD_LOGIC;
    s_axis_tready : OUT STD_LOGIC;
    s_axis_tdata  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_tlast  : IN  STD_LOGIC;
    m_axis_tvalid : OUT STD_LOGIC;
    m_axis_tready : IN  STD_LOGIC;
    m_axis_tdata  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    m_axis_tlast  : OUT STD_LOGIC
  );
  end component Capitalizator;


  signal udp_tx_start          : std_logic;
  signal udp_txi               : udp_tx_type;
  signal udp_tx_result         : std_logic_vector (1 downto 0);
  signal udp_tx_data_out_ready : std_logic;
  signal udp_rx_start          : std_logic;
  signal udp_rxo               : udp_rx_type;
  signal ip_rx_hdr             : ipv4_rx_header_type;
  signal our_ip_address        : STD_LOGIC_VECTOR (31 downto 0);
  signal our_mac_address       : std_logic_vector (47 downto 0);
  signal control               : udp_control_type;
  signal arp_pkt_count         : STD_LOGIC_VECTOR(7 downto 0);
  signal ip_pkt_count          : STD_LOGIC_VECTOR(7 downto 0);
  signal udp_txi_hdr_data_length : std_logic_vector(10 downto 0);
  signal dst_ip_addr     : std_logic_vector (31 downto 0);
  signal dst_port        : std_logic_vector (15 downto 0);
  signal src_port        : std_logic_vector (15 downto 0);
  signal filter_port        : std_logic_vector (15 downto 0);

  signal upd_packet_data_in_valid : std_logic;

  signal packet_rx_tdata          : std_logic_vector(7 downto 0);
  signal packet_rx_tvalid         : std_logic;
  signal packet_rx_tready         : std_logic;
  signal packet_rx_tlast          : std_logic;								-- indicates last byte of the trame;


  signal capitalizator_tdata          : std_logic_vector(7 downto 0);
  signal capitalizator_tvalid         : std_logic;
  signal capitalizator_tready         : std_logic;
  signal capitalizator_tlast          : std_logic;								-- indicates last byte of the trame;

  signal packet_tx_tdata          : std_logic_vector(7 downto 0);
  signal packet_tx_tvalid         : std_logic;
  signal packet_tx_tready         : std_logic;
  signal packet_tx_tlast          : std_logic;								-- indicates last byte of the trame;


  attribute mark_debug : string;
  attribute keep : string;
  attribute dont_touch : string;
  attribute keep_hierarchy : string;

  attribute mark_debug of udp_tx_data_out_ready : signal is "true";
  attribute keep       of udp_tx_data_out_ready : signal is "true";
  attribute dont_touch of udp_tx_data_out_ready : signal is "true";
  attribute keep_hierarchy of udp_tx_data_out_ready : signal is "true";
  attribute mark_debug of udp_tx_start : signal is "true";
  attribute keep       of udp_tx_start : signal is "true";
  attribute dont_touch of udp_tx_start : signal is "true";
  attribute keep_hierarchy of udp_tx_start : signal is "true";
  attribute mark_debug of udp_txi : signal is "true";
  attribute keep       of udp_txi : signal is "true";
  attribute dont_touch of udp_txi : signal is "true";
  attribute keep_hierarchy of udp_txi : signal is "true";
  attribute mark_debug of udp_tx_result : signal is "true";
  attribute keep       of udp_tx_result : signal is "true";
  attribute dont_touch of udp_tx_result : signal is "true";
  attribute keep_hierarchy of udp_tx_result : signal is "true";
  attribute mark_debug of udp_rxo : signal is "true";
  attribute keep       of udp_rxo : signal is "true";
  attribute dont_touch of udp_rxo : signal is "true";
  attribute keep_hierarchy of udp_rxo : signal is "true";
  attribute mark_debug of ip_rx_hdr : signal is "true";
  attribute keep       of ip_rx_hdr : signal is "true";
  attribute dont_touch of ip_rx_hdr : signal is "true";
  attribute keep_hierarchy of ip_rx_hdr : signal is "true";
  attribute mark_debug of our_ip_address : signal is "true";
  attribute keep       of our_ip_address : signal is "true";
  attribute dont_touch of our_ip_address : signal is "true";
  attribute keep_hierarchy of our_ip_address : signal is "true";
  attribute mark_debug of our_mac_address : signal is "true";
  attribute keep       of our_mac_address : signal is "true";
  attribute dont_touch of our_mac_address : signal is "true";
  attribute keep_hierarchy of our_mac_address : signal is "true";
  attribute mark_debug of control : signal is "true";
  attribute keep       of control : signal is "true";
  attribute dont_touch of control : signal is "true";
  attribute keep_hierarchy of control : signal is "true";
  attribute mark_debug of arp_pkt_count : signal is "true";
  attribute keep       of arp_pkt_count : signal is "true";
  attribute dont_touch of arp_pkt_count : signal is "true";
  attribute keep_hierarchy of arp_pkt_count : signal is "true";
  attribute mark_debug of ip_pkt_count : signal is "true";
  attribute keep       of ip_pkt_count : signal is "true";
  attribute dont_touch of ip_pkt_count : signal is "true";
  attribute keep_hierarchy of ip_pkt_count : signal is "true";
begin
  UDP_Complete_nomac_i : UDP_Complete_nomac
  generic map (
    CLOCK_FREQ      => 125000000,
    ARP_TIMEOUT     => 60,
    ARP_MAX_PKT_TMO => 5,
    MAX_ARP_ENTRIES => 255
  )
  port map (
    udp_tx_start          => udp_tx_start,
    udp_txi               => udp_txi,
    udp_tx_result         => udp_tx_result,
    udp_tx_data_out_ready => udp_tx_data_out_ready,
    udp_rx_start          => udp_rx_start,
    udp_rxo               => udp_rxo,
    ip_rx_hdr             => ip_rx_hdr,
    rx_clk                => rx_clk,
    tx_clk                => tx_clk,
    reset                 => reset,
    our_ip_address        => our_ip_address,
    our_mac_address       => our_mac_address,
    control               => control,
    arp_pkt_count         => arp_pkt_count,
    ip_pkt_count          => ip_pkt_count,
    mac_tx_tdata          => mac_tx_tdata,
    mac_tx_tvalid         => mac_tx_tvalid,
    mac_tx_tready         => mac_tx_tready,
    mac_tx_tfirst         => open,
    mac_tx_tlast          => mac_tx_tlast,
    mac_rx_tdata          => mac_rx_tdata,
    mac_rx_tvalid         => mac_rx_tvalid,
    mac_rx_tready         => mac_rx_tready,
    mac_rx_tlast          => mac_rx_tlast
  );

  UPD_PACKET_IN_FILTER : process(udp_rxo.hdr.dst_port, filter_port, udp_rxo.data.data_in_valid)
  begin
    upd_packet_data_in_valid <= (not OR_REDUCE(udp_rxo.hdr.dst_port xor filter_port)) and udp_rxo.data.data_in_valid;
  end process; -- end UPD_PACKET_IN_FILTER

  rx_packet_fifo : packet_fifo
    PORT MAP (
      s_aclk => rx_clk,
      s_aresetn => mac_rx_rstn,
      s_axis_tvalid => upd_packet_data_in_valid,
      s_axis_tready => open,
      s_axis_tdata =>  udp_rxo.data.data_in,
      s_axis_tlast =>  udp_rxo.data.data_in_last,
      m_axis_tvalid => packet_rx_tvalid,
      m_axis_tready => packet_rx_tready,
      m_axis_tdata =>  packet_rx_tdata,
      m_axis_tlast =>  packet_rx_tlast,
      axis_data_count => open
    );

  Capitalizator_i : Capitalizator
  port map (
    s_axis_tvalid => packet_rx_tvalid,
    s_axis_tready => packet_rx_tready,
    s_axis_tdata  => packet_rx_tdata,
    s_axis_tlast  => packet_rx_tlast,
    m_axis_tvalid => capitalizator_tvalid,
    m_axis_tready => capitalizator_tready,
    m_axis_tdata  => capitalizator_tdata,
    m_axis_tlast  => capitalizator_tlast
  );

  rx_to_tx_fifo : cross_clock_packet_fifo
    PORT MAP (
      m_aclk => tx_clk,
      s_aclk => rx_clk,
      s_aresetn => mac_rx_rstn,
      s_axis_tvalid => capitalizator_tvalid,
      s_axis_tready => capitalizator_tready,
      s_axis_tdata =>  capitalizator_tdata,
      s_axis_tlast =>  capitalizator_tlast,
      m_axis_tvalid => packet_tx_tvalid,
      m_axis_tready => packet_tx_tready,
      m_axis_tdata =>  packet_tx_tdata,
      m_axis_tlast =>  packet_tx_tlast
    );

  tx_packet_fifo : packet_fifo
    PORT MAP (
      s_aclk => tx_clk,
      s_aresetn => mac_tx_rstn,
      s_axis_tvalid => packet_tx_tvalid,
      s_axis_tready => packet_tx_tready,
      s_axis_tdata =>  packet_tx_tdata,
      s_axis_tlast =>  packet_tx_tlast,
      m_axis_tvalid => udp_txi.data.data_out_valid,
      m_axis_tready => udp_tx_data_out_ready,
      m_axis_tdata =>  udp_txi.data.data_out,
      m_axis_tlast =>  udp_txi.data.data_out_last,
      axis_data_count => udp_txi_hdr_data_length
    );
    udp_tx_start  <= udp_txi.data.data_out_valid;
		control.ip_controls.arp_controls.clear_cache <= '0';

    udp_txi.hdr.dst_ip_addr <= dst_ip_addr;
    udp_txi.hdr.dst_port <= dst_port;
    udp_txi.hdr.src_port <= src_port;
    udp_txi.hdr.data_length <= std_logic_vector(resize(unsigned(udp_txi_hdr_data_length), udp_txi.hdr.data_length'length));
    udp_txi.hdr.checksum <= (others => '0');

    app_axi_configuration_i : app_axi_configuration
    generic map (
      C_S_AXI_DATA_WIDTH    => C_S_AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH    => C_S_AXI_ADDR_WIDTH,
      C_DEF_OUR_IP_ADDRESS  => x"c0a8586f",
      C_DEF_OUR_MAC_ADDRESS => x"0605040302DA",
      C_DEF_DST_IP_ADDRESS  => x"c0a858d7",
      C_DEF_DST_PORT        => std_logic_vector(to_unsigned(9100, 16)),
      C_DEF_SRC_PORT        => std_logic_vector(to_unsigned(9200, 16)),
      C_DEF_FILTER_PORT     => std_logic_vector(to_unsigned(9200, 16))
    )
    port map (
      our_ip_address  => our_ip_address,
      our_mac_address => our_mac_address,
      dst_ip_addr     => dst_ip_addr,
      dst_port        => dst_port,
      src_port        => src_port,
      filter_port     => filter_port,
      S_AXI_ACLK      => app_config_ACLK,
      S_AXI_ARESETN   => app_config_ARESETN,
      S_AXI_AWADDR    => app_config_AWADDR,
      S_AXI_AWPROT    => app_config_AWPROT,
      S_AXI_AWVALID   => app_config_AWVALID,
      S_AXI_AWREADY   => app_config_AWREADY,
      S_AXI_WDATA     => app_config_WDATA,
      S_AXI_WSTRB     => app_config_WSTRB,
      S_AXI_WVALID    => app_config_WVALID,
      S_AXI_WREADY    => app_config_WREADY,
      S_AXI_BRESP     => app_config_BRESP,
      S_AXI_BVALID    => app_config_BVALID,
      S_AXI_BREADY    => app_config_BREADY,
      S_AXI_ARADDR    => app_config_ARADDR,
      S_AXI_ARPROT    => app_config_ARPROT,
      S_AXI_ARVALID   => app_config_ARVALID,
      S_AXI_ARREADY   => app_config_ARREADY,
      S_AXI_RDATA     => app_config_RDATA,
      S_AXI_RRESP     => app_config_RRESP,
      S_AXI_RVALID    => app_config_RVALID,
      S_AXI_RREADY    => app_config_RREADY
    );
end Behavioral;
